package co.com.colfondos.txtvalidator;

import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Validation {

    List<String> errors = new ArrayList<>();

    public void preValidate(String text) {
        List<String> invalidCharacters = Arrays.asList("?", "!", "*", "%", "&", ".",
                ",", "(", ")", "/", "=", "+", "ñ",
                ":", "’", "\'", ";", "@", "_", "-","á","Á","é","É","í","Í","ó","Ó","ú","Ú");

        List<Integer> indexInvalid = new ArrayList<>();
        for (int i = 0; i < text.length(); i++) {
            if(invalidCharacters.contains(text.substring(i, i +1))) {
                indexInvalid.add(i+1);
            }
        }
        if (!indexInvalid.isEmpty()) {
            errors.add("se encontro un caracter especial en: "+ indexInvalid.toString());
        }
    }

    public List<String> validate(Map<String, String> splittedText) {

        //se ponen las validaciones

        validateRegistro(splittedText.get("TIPO_DE_REGISTRO"), errors);
        validatePais(splittedText.get("CODIGO_DEL_PAIS"),errors);
        validateCuentaCliente(splittedText.get("CUENTA_DEL_CLIENTE"),errors);
        validateFechaDebito(splittedText.get("FECHA_DEL_DEBITO"),errors);
        validateTipoDebito(splittedText.get("TIPO_DEL_DEBITO"),errors);
        validateReferenciaCliente(splittedText.get("REFERENCIA DEL CLIENTE"),errors);
        validateSecuencia(splittedText.get("SECUENCIA"),errors);
        validateNIT(splittedText.get("NIT"),errors);
        validateCodigoMoneda(splittedText.get("CODIGO_DE_LA_MONEDA"),errors);
        validateCodigoBeneficiario(splittedText.get("CODIGO_DEL_BENEFICIARIO"),errors);
        validateMontoDebitar(splittedText.get("MONTO_A_DEBITAR"),errors);
        validateFechaVencimiento(splittedText.get("FECHA_DE_VENCIMIENTO"),errors);
        validatedescripcion1(splittedText.get("DESCRIPCION-1"),errors);
        validateTipoDocTransferencia(splittedText.get("TIPO_DOC_AUTORIZACION_TRANSFERENCIA"),errors);
        validateCuentaColfondos(splittedText.get("TIPO_CUENTA_COLFONDOS"),errors);
        validateNombreBeneficiario(splittedText.get("NOMBRE_BENEFICIARIO"),errors);
        validateDireccionBeneficiario(splittedText.get("DIRECCION_BENEFICIARIO"),errors);
        validateCodigoPostalBeneficiario(splittedText.get("CODIGO_POSTAL_BENEFICIARIO"),errors);
        validateTelefonoBeneficiario(splittedText.get("TELEFONO_BENEFICIARIO"),errors);
        validateNumeroBanco(splittedText.get("NUMERO_BANCO"),errors);
        validateAgenciaBanco(splittedText.get("AGENCIA_BANCO"),errors);
        validateCuentaBancaria(splittedText.get("CUENTA_BANCARIA"),errors);
        validateTipoCuenta(splittedText.get("TIPO_CUENTA"),errors);
        validateDireccionBancoBeneficiario(splittedText.get("DIRECCION_BANCO_BENEFICIARIO"),errors);
        validateFax(splittedText.get("NUMERO_FAX"),errors);
        validateCuentaBeneficiarioCitibank(splittedText.get("CUENTA_BENEFICIARIO_CITIBANK"),errors);
        validateTipoCuentaCitibank(splittedText.get("TIPO_CUENTA_BENEFICIARIO_CITIBANK"),errors);
        validateSucursalDestino(splittedText.get("SUCURSAL_DESTINO"),errors);
        validateValorMaximoDebito(splittedText.get("VALOR MAXIMO_DEBITO"),errors);

        /*Validaciones registro TRL

        validateTipoRegistro(splittedText.get("TIPO REGISTRO"),errors);
        validateRegistrosDebito(splittedText.get("NUMERO_DE_REGISTROS"),errors);
        */


      //  validartodo el archivo
        //si no tiene ningun error se muestra mensaje diciendo que esta correcto
        if(errors.isEmpty()) {
            errors.add("Registro PAY es valido!");
        }
        return errors;
}

    public List<String> validatet (Map<String, String> splittedText){

        validatetTipoRegistro(splittedText.get("TIPO REGISTRO"),errors);
        validatetRegistrosDebito(splittedText.get("NUMERO_DE_REGISTROS"),errors);
        validatetMontoTotalDebitar(splittedText.get("MONTO_TOTAL"),errors);
        validatetRegistrosBeneficiario(splittedText.get("NUMERO_DE_REGISTROS_BENEFICIARIO"),errors);
        validatetRegistrosEnviados(splittedText.get("NUMERO_REGISTROS_ENVIADOS"),errors);


        if(errors.isEmpty()) {
            errors.add("TRL es valido!");
        }
        return errors;
    }


    private void validateRegistro(String registro, List<String> errors) {

        if(StringUtils.isBlank(registro)) {
            errors.add("Tipo de registro es obligatorio");
        } else if(!"PAY".equals(registro)) {
            errors.add ("El valor es diferente a PAY-Debitos, valor encontrado: " + registro);
        }
    }


    private void validatePais(String pais, List<String> errors) {
        if(StringUtils.isBlank(pais)) {
            errors.add("pais es obligatorio");
        } else if(isNotNumber(pais)) {
            errors.add("pais debe ser numerico");
        } else if (!"170".equals(pais)){
           errors.add("El país del cliente debitador no es Colombia,valor encontrado: " + pais);
        }
    }

    private void validateCuentaCliente(String cuenta_del_cliente, List<String> errors) {
        if(StringUtils.isBlank(cuenta_del_cliente)) {
            errors.add("cuenta del cliente es obligatorio");
        } else if(isNotNumber(cuenta_del_cliente)) {
            errors.add("cuenta debe ser numerico,valor encontrado: " + cuenta_del_cliente);
        }
    }

    private void validateFechaDebito(String fecha_del_debito, List<String> errors) {
        if(StringUtils.isBlank(fecha_del_debito)) {
            errors.add("fecha del debito es obligatorio");
        } else if(!validarFecha(fecha_del_debito)) {
            errors.add("la fecha del débito no se presenta en formato AAMMDD, valor encontrado: "+ fecha_del_debito);
        }
    }

    private void validateTipoDebito(String tipoDelDebito, List<String> errors) {
        if(StringUtils.isBlank(tipoDelDebito)){
            errors.add("tipo del debito es obligatorio");
        }   else if (!"071".equals(tipoDelDebito)) {
            errors.add("Corresponde a una transacción de Citibank a Citibank,valor encontrado: " + tipoDelDebito );
        }
    }

    private void validateReferenciaCliente(String referencia_del_cliente, List<String> errors) {
        if(StringUtils.isBlank(referencia_del_cliente)) {
            errors.add("referencia del cliente es obligatorio");
        }
    }

    private void validateSecuencia(String secuencia, List<String> errors) {
        if(StringUtils.isBlank(secuencia)) {
            errors.add("secuencia es obligatorio");
        }
    }

    private void validateNIT(String nit, List<String> errors) {
        if(StringUtils.isBlank(nit)) {
            errors.add("NIT es obligatorio");
        }
    }

    private void validateCodigoMoneda(String codigo_de_la_moneda, List<String> errors) {
        if (StringUtils.isBlank(codigo_de_la_moneda)) {
            errors.add("codigo de la moneda es obligatorio");
        } else if (!"COP".equals(codigo_de_la_moneda)) {
            errors.add("La moneda del débito no es para Colombia,valor encontrado: "+ codigo_de_la_moneda);
        }
    }

    private void validateCodigoBeneficiario(String codigo_del_beneficiario, List<String> errors) {
        if (StringUtils.isBlank(codigo_del_beneficiario)) {
            errors.add("codigo del beneficiario es obligatorio");
        }
    }

    private void validateMontoDebitar(String monto_a_debitar, List<String> errors) {
        if (StringUtils.isBlank(monto_a_debitar)) {
            errors.add("codigo del beneficiario es obligatorio");
        }
    }

    private void validateFechaVencimiento(String fecha_de_vencimiento, List<String> errors) {
        if(!StringUtils.isBlank(fecha_de_vencimiento)) {
            if(!validarFecha(fecha_de_vencimiento)){
                errors.add("fecha de vencimiento no tiene el formato AAMMDD: "+ fecha_de_vencimiento );
            }
        }
    }

    private void validatedescripcion1(String descripcion1, List<String> errors) {
        if(!StringUtils.isBlank(descripcion1)) {
            if (!sameLengthAfterTrim(descripcion1)) {
                errors.add("el campo no debe ser completado con espacios: " + descripcion1);
            }
        }
    }

    private void validateTipoDocTransferencia(String tipo_doc_autorizacion_transferencia, List<String> errors) {
        if (StringUtils.isBlank(tipo_doc_autorizacion_transferencia)) {
            errors.add("tipo documento autorización transferencia es obligatorio");
        } else if (isNotNumber(tipo_doc_autorizacion_transferencia)) {
            errors.add("tipo documento autorización transferencia debe ser numerico");
        } else if (!"46".equals(tipo_doc_autorizacion_transferencia)) {
            errors.add("tipo documento autorización transferencia no es para débito: " + tipo_doc_autorizacion_transferencia);
        }
    }

    private void validateCuentaColfondos(String tipo_cuenta_colfondos, List<String> errors) {
        if (StringUtils.isBlank(tipo_cuenta_colfondos)) {
            errors.add("tipo cuenta Colfondos es obligatorio");
        } else if (!("01".equals(tipo_cuenta_colfondos) || "02".equals(tipo_cuenta_colfondos))){
            errors.add("tipo de cuenta no es 01=corriente y tampoco 02=ahorros: "+ tipo_cuenta_colfondos);
        }
    }

    private void validateNombreBeneficiario(String nombre_beneficiario, List<String> errors) {
        if (StringUtils.isBlank(nombre_beneficiario)) {
            errors.add("nombre del beneficiario es obligatorio");
        }
    }

    private void validateDireccionBeneficiario(String direccion_beneficiario, List<String> errors) {
        if (StringUtils.isBlank(direccion_beneficiario)) {
            errors.add("direccion del beneficiario es obligatorio");
        }
    }


    private void validateCodigoPostalBeneficiario(String codigo_postal_beneficiario, List<String> errors) {
        if (!StringUtils.isBlank(codigo_postal_beneficiario)) {
            if (isNotNumber(codigo_postal_beneficiario)) {
                errors.add("codigo postal debe ser numerico,valor encontrado: "+ codigo_postal_beneficiario);
            }
        }
    }


    // numerico opcional
    private void validateTelefonoBeneficiario(String telefono_beneficiario, List<String> errors) {
        if (!StringUtils.isBlank(telefono_beneficiario)) {
            if (isNotNumber(telefono_beneficiario)) {
                errors.add("telefono debe ser numerico,valor encontrado: "+ telefono_beneficiario);
            }
        }
    }

    private void validateNumeroBanco(String numero_banco, List<String> errors) {
        List<String> bancos = Arrays.asList("001", "002", "006", "007", "012",
                "013", "019", "023", "032", "040", "051", "052", "060", "061",
                "062");
        if (StringUtils.isBlank(numero_banco)) {
            errors.add("número del banco es obligatorio");
        } else if (!bancos.contains(numero_banco)) {
            errors.add("numero de banco: " + numero_banco +
                " no corresponde con los valores definidos: "+ bancos.toString());
        }
    }

    private void validateAgenciaBanco(String agencia_banco, List<String> errors) {
        if(StringUtils.isBlank(agencia_banco)){
            errors.add("agencia del banco es obligatorio");
        }   else if (!"0001    ".equals(agencia_banco)) {
            errors.add("La dirección del banco si es conocida: " + agencia_banco);
        }
    }


    private void validateCuentaBancaria(String cuenta_bancaria, List<String> errors) {
        if (StringUtils.isBlank(cuenta_bancaria)) {
            errors.add("cuenta bancaria es obligatorio");
        }
    }

    private void validateTipoCuenta(String tipo_cuenta, List<String> errors) {
        if (StringUtils.isBlank(tipo_cuenta)) {
            errors.add("tipo cuenta Colfondos es obligatorio");
        } else if (!("01".equals(tipo_cuenta) || "02".equals(tipo_cuenta))) {
            errors.add("tipo de cuenta no es 01=corriente y tampoco 02=ahorros: " + tipo_cuenta);
        }
    }

    private void validateDireccionBancoBeneficiario(String direccion_banco_beneficiario, List<String> errors) {
        if(StringUtils.isBlank(direccion_banco_beneficiario)) {
            errors.add("Direccion del banco beneficario es obligatorio");
        } else if("PPAL".equals(direccion_banco_beneficiario)) {
            errors.add ("Direccion del banco beneficiario es diferente, valor encontrado: " + direccion_banco_beneficiario);
        }
    }

    private void validateFax(String numero_fax, List<String> errors) {
        if (!StringUtils.isBlank(numero_fax)) {
            if (isNotNumber(numero_fax)) {
                errors.add("numero fax debe ser numerico,valor encontrado: "+ numero_fax);
            }
        }
    }

    private void validateCuentaBeneficiarioCitibank(String cuenta_beneficiario_citibank, List<String> errors) {
        if (!StringUtils.isBlank(cuenta_beneficiario_citibank)) {
            if (isNotNumber(cuenta_beneficiario_citibank)) {
                errors.add("cuenta del beneficiario en Citibank debe ser numerico,valor encontrado: "+ cuenta_beneficiario_citibank);
            }
        }
    }

    private void validateTipoCuentaCitibank(String tipo_cuenta_beneficiario_citibank, List<String> errors) {
        if (!StringUtils.isBlank(tipo_cuenta_beneficiario_citibank)) {
            if (!("01".equals(tipo_cuenta_beneficiario_citibank) || "02".equals(tipo_cuenta_beneficiario_citibank))) {
                errors.add("Tipo de cuenta para beneficiarios en Citibank no es 01=corriente y tampoco 02_ahorros,valor encontrado: "+ tipo_cuenta_beneficiario_citibank);
            }
        }
    }

    private void validateSucursalDestino(String sucursal_destino, List<String> errors) {
        if(StringUtils.isBlank(sucursal_destino)) {
            errors.add("Sucursal destino es obligatorio");
        } else if(!"116".equals(sucursal_destino)) {
            errors.add ("Sucursal destino es diferente, valor encontrado: " + sucursal_destino);
        }
    }


    private void validateValorMaximoDebito(String valor_maximo_debito, List<String> errors) {
        if(StringUtils.isBlank(valor_maximo_debito)) {
            errors.add("valor maximo debito es obligatorio");
        } else if(isNotNumber(valor_maximo_debito)) {
            errors.add("valor maximo debito debe ser numerico");
        }


    }

    //Métodos TRL

    private void validatetTipoRegistro(String tipo_registro, List<String> errors) {
        if(StringUtils.isBlank(tipo_registro)) {
            errors.add("Tipo de registro es obligatorio");
        } else if(!"TRL".equals(tipo_registro)) {
            errors.add ("El valor es diferente a TRL, valor encontrado: " + tipo_registro);
        }
    }

    private void validatetRegistrosDebito(String numero_de_registros, List<String> errors) {
        if(StringUtils.isBlank(numero_de_registros)) {
            errors.add("número de registros de debito es obligatorio");
        } else if(isNotNumber(numero_de_registros)) {
            errors.add("numero de registros de debito debe ser numerico");
        }
    }
    private void validatetMontoTotalDebitar(String monto_total, List<String> errors) {
        if(StringUtils.isBlank(monto_total)) {
            errors.add("monto total a debitar es obligatorio");
        } else if(isNotNumber(monto_total)) {
            errors.add("monto total a debitar debe ser numerico");
        }
    }

    private void validatetRegistrosBeneficiario(String numero_de_registros_beneficiario, List<String> errors) {
        if(StringUtils.isBlank(numero_de_registros_beneficiario)) {
            errors.add("numero de registros de beneficiario es obligatorio");
        } else if(isNotNumber(numero_de_registros_beneficiario)) {
            errors.add("numero de registros de beneficiario debe ser numerico");
        }
    }

    private void validatetRegistrosEnviados(String numero_registros_enviados, List<String> errors) {
        if(StringUtils.isBlank(numero_registros_enviados)) {
            errors.add("numero de registros enviados es obligatorio");
        } else if(isNotNumber(numero_registros_enviados)) {
            errors.add("numero de registros enviados debe ser numerico");
        }
    }


        private boolean sameLengthAfterTrim(String field) {
        String fieldAfterTrim = field.trim();
        return field.length() == fieldAfterTrim.length();
    }

    // Metodos genericos
    private boolean isNumber(String number) {
        try {
            Long.parseLong(number);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isNotNumber(String number) {
        return !isNumber(number);
    }

    private boolean validarFecha (String fecha) {
        try {
        SimpleDateFormat formatoFecha = new SimpleDateFormat("yyMMdd");
        formatoFecha.setLenient(false);
        formatoFecha.parse(fecha) ;
        } catch (ParseException e) {
            return false;
        }
        return true;
    }

   /*private boolean sumatoriapay (String registro) {
       int sumaregistros=0;

   }
*/



}


