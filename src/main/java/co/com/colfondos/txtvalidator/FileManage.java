package co.com.colfondos.txtvalidator;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class FileManage {

    //public String readFile() throws IOException {
    public List<String> readFile() throws IOException {
        File file = new File("C:\\Users\\II69345\\Documents\\PAOLA\\DEBITO_AUTOMATICO\\Sprint5\\Validador\\text.txt");

       // BufferedReader br = new BufferedReader(new FileReader(file));
       //return br.readLine();


       return Files.lines(Paths
                .get("C:\\Users\\II69345\\Documents\\PAOLA\\DEBITO_AUTOMATICO\\Sprint5\\Validador\\text.txt"),
                StandardCharsets.UTF_8).collect(Collectors.toList());
    }

    public String[] preValidate(String text) {
        return text.split("\n");
    }


    public Map<String, String> processFile(String text) {
       int longitudTexto = 1024;
       if(text.length() < longitudTexto){
           throw new RuntimeException("el numero de caracteres es inferior a: "+ longitudTexto);
        }
        Map<String, String> splittedText = new HashMap<>();
        int cursor = 0;
        splittedText.put("TIPO_DE_REGISTRO", text.substring(cursor, cursor = cursor + 3));
        splittedText.put("CODIGO_DEL_PAIS", text.substring(cursor, cursor = cursor + 3));
        splittedText.put("CUENTA_DEL_CLIENTE", text.substring(cursor, cursor = cursor + 10));
        splittedText.put("FECHA_DEL_DEBITO", text.substring(cursor, cursor = cursor + 6));
        splittedText.put("TIPO_DEL_DEBITO", text.substring(cursor, cursor = cursor + 3));
        splittedText.put("REFERENCIA DEL CLIENTE", text.substring(cursor, cursor = cursor + 15));
        splittedText.put("SECUENCIA", text.substring(cursor, cursor = cursor + 8));
        splittedText.put("NIT", text.substring(cursor, cursor = cursor + 20));
        splittedText.put("CODIGO_DE_LA_MONEDA", text.substring(cursor, cursor = cursor + 3));
        splittedText.put("CODIGO_DEL_BENEFICIARIO", text.substring(cursor, cursor = cursor + 20));
        splittedText.put("MONTO_A_DEBITAR", text.substring(cursor, cursor = cursor + 15));
        splittedText.put("FECHA_DE_VENCIMIENTO", text.substring(cursor, cursor = cursor + 6));
        splittedText.put("DESCRIPCION-1", text.substring(cursor, cursor = cursor + 13));
        splittedText.put("DESCRIPCION-1-1", text.substring(cursor, cursor = cursor + 22));
        splittedText.put("DESCRIPCION-2", text.substring(cursor, cursor = cursor + 35));
        splittedText.put("DESCRIPCION-3", text.substring(cursor, cursor = cursor + 35));
        splittedText.put("DESCRIPCION-4", text.substring(cursor, cursor = cursor + 35));
        splittedText.put("TIPO_DOC_AUTORIZACION_TRANSFERENCIA", text.substring(cursor, cursor = cursor + 2));
        splittedText.put("TIPO_CUENTA_COLFONDOS", text.substring(cursor, cursor = cursor + 2));
        splittedText.put("NOMBRE_BENEFICIARIO", text.substring(cursor, cursor = cursor + 80));
        splittedText.put("DIRECCION_BENEFICIARIO", text.substring(cursor, cursor = cursor + 35));
        splittedText.put("DIRECCION_BENEFICIARIO_2", text.substring(cursor, cursor = cursor + 35));
        splittedText.put("CIUDAD_BENEFICIARIO", text.substring(cursor, cursor = cursor + 15));
        splittedText.put("DEPARTAMENTO_BENEFICIARIO", text.substring(cursor, cursor = cursor + 2));
        splittedText.put("CODIGO_POSTAL_BENEFICIARIO", text.substring(cursor, cursor = cursor + 12));
        splittedText.put("TELEFONO_BENEFICIARIO", text.substring(cursor, cursor = cursor + 16));
        splittedText.put("NUMERO_BANCO", text.substring(cursor, cursor = cursor + 3));
        splittedText.put("AGENCIA_BANCO", text.substring(cursor, cursor = cursor + 8));
        splittedText.put("CUENTA_BANCARIA", text.substring(cursor, cursor = cursor + 35));
        splittedText.put("TIPO_CUENTA", text.substring(cursor, cursor = cursor + 2));
        splittedText.put("DIRECCION_BANCO_BENEFICIARIO", text.substring(cursor, cursor = cursor + 30));
        splittedText.put("RESERVADO_USO_FUTURO", text.substring(cursor, cursor = cursor + 41));
        splittedText.put("NUMERO_FAX", text.substring(cursor, cursor = cursor + 16));
        splittedText.put("NOMBRE_FAX", text.substring(cursor, cursor = cursor + 20));
        splittedText.put("DEPARTAMENTO_FAX", text.substring(cursor, cursor = cursor + 15));
        splittedText.put("CUENTA_BENEFICIARIO_CITIBANK", text.substring(cursor, cursor = cursor + 10));
        splittedText.put("TIPO_CUENTA_BENEFICIARIO_CITIBANK", text.substring(cursor, cursor = cursor + 2));
        splittedText.put("SUCURSAL_DESTINO", text.substring(cursor, cursor = cursor + 3));
        splittedText.put("ID_COBRANZA", text.substring(cursor, cursor = cursor + 50));
        splittedText.put("CODIGO_ACTIVO_BENEFICIARIO", text.substring(cursor, cursor = cursor + 5));
        splittedText.put("EMAIL_BENEFICIARIO", text.substring(cursor, cursor = cursor + 50));
        splittedText.put("VALOR MAXIMO_DEBITO", text.substring(cursor, cursor = cursor + 15));
        splittedText.put("RESERVADO", text.substring(cursor, cursor = cursor + 267));



        return splittedText;
    }


    public Map<String, String> processTRL(String text) {
        int longitudTexto = 101;
        if(text.length() < longitudTexto){
            throw new RuntimeException("el numero de caracteres es inferior a: "+ longitudTexto);
        }

        Map<String, String> splittedText = new HashMap<>();
        int cursor = 0;

        // Registro TRL

        splittedText.put("TIPO REGISTRO", text.substring(cursor, cursor = cursor + 3));
        splittedText.put("NUMERO_DE_REGISTROS", text.substring(cursor, cursor = cursor + 15));
        splittedText.put("MONTO_TOTAL", text.substring(cursor, cursor = cursor + 15));
        splittedText.put("NUMERO_DE_REGISTROS_BENEFICIARIO", text.substring(cursor, cursor = cursor + 15));
        splittedText.put("NUMERO_REGISTROS_ENVIADOS", text.substring(cursor, cursor = cursor + 15));
        splittedText.put("RESERVADO", text.substring(cursor, cursor = cursor + 37));
        return splittedText;
    }
}
