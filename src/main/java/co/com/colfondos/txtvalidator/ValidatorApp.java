package co.com.colfondos.txtvalidator;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ValidatorApp {
	public static void main(String[] args) throws IOException {
		//se lee el archivo txt
		FileManage fm = new FileManage();
		//String txtText=fm.readFile();
		List<String> txtText = fm.readFile();
		//Se separa el txt en las diferentes filas
		//String[] splittedText = fm.preValidate(txtText);

		for (int i = 0; i < txtText.size()-1; i++) {
			Map<String, String> mappedText = fm.processFile(txtText.get(i));
			System.out.println("Se va a validar la fila: " + (i+1));
			//se validan los parametros
			Validation validation = new Validation();
			validation.preValidate(txtText.get(i));
			List<String> errores = validation.validate(mappedText);
			//se imprimen en consola los errores obtenidos
			errores.stream().forEach(System.out::println);

		}


	Map<String, String> mappedText = fm.processTRL(txtText.get(txtText.size()-1));
		System.out.println("Se va a validar el TRL ");
		Validation validationt = new Validation();
		List<String> erroresTRL = validationt.validatet(mappedText);
		erroresTRL.stream().forEach(System.out::println);
  }


	}
